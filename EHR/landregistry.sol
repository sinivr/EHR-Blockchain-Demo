pragma solidity ^0.4.0;
contract LandReg{
    
     
     mapping (bytes32 => bool) public proofs;
     
     
      function calculateProof(string document) constant returns (bytes32) {
      return sha256(document);
  }
     
     
     function storeProof(bytes32 proof) {
    proofs[proof] = true;
  }
  
  
  function notarize(string document) {
    var proof = calculateProof(document);
    storeProof(proof);
  }
  
  
   function hasProof(bytes32 proof) constant returns(bool) {
    return proofs[proof];
   }
   
   
   function checkDocument(string document) constant returns (bool) {
    var proof = calculateProof(document);
    return hasProof(proof);
  }
  
}
  
    

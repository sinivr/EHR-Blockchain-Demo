"""
Crafting a new ICMP packet from scratch 
for making ping request to servers
TODO:
1. Craft a ICMP packet
2. Make request to a client/ server
3. Find all the systems in the network
"""
"""
Title: Craft and send ICMP packets
Author: Jithin Zacharia
Git: github.com/Jithinqw
"""
import os,sys
import socket,struct
import select,time

ICMP_ECHO_REQUEST=8 #type of ICMP packet message is 8 bytes
source_address=raw_input("Enter the source address")
destination_address=raw_input("Enter the destination address")

#function for creating a socket
def create():
	print "I am UP"
	icmp=socket.getprotobyname("icmp")
	try:
		s=socket.socket(socket.AF_INET,socket.SOCK_RAW,icmp)
	except socket.error, (error_num,msg):
		if error_num==1:
			msg=msg+("Operation not permitted, Check if you're in super-user mode")
			raise socket.error(msg)
		raise #raise original msg from socket
	ID=os.getpid() & 0xFFFF #getting the current ProcessID multithreaded program error 
	craft_packet(destination_address,ID)
	delay=recieve(s,my_ID,timeout)
	s.close()
	return delay

#function for revieving the ping rsults
def recieve(s,my_ID,timeout):
	timeLeft =timeout
	while True:
		startedSelect=time.time()
		whatReady=select.select([my_socket],[],[],timeLeft)
		howLongInSelect=(time.time()-startedSelect)
		if whatReady[0]==[]:#retuns Timeout
			return
		timeRecived=time.time()
		recPacket,addr=s.recvfrom(6634)
		icmpHeader=recPacket[20:28]
		type,code,checksum,packetID,sequence=struct.unpack("blahh",icmpHeader)
		if packetID==ID:
			bytesInDouble=struct.calcsize("d")
			timeSent=struct.unpack("d",recpacket[28:28 + bytesInDouble])[0]
			return timeRecieved-timeSent
		timeLeft=timeLeft-howLongInSelect
		if timeLeft<=0:
			return 
#function for crafting the packet
def craft_packet(destination_address,ID):
	#making header with checksum 0
	my_checksum=0
	#header=type+code+checksum+id+sequence
	header=struct.pack("blahh",ICMP_ECHO_REQUEST,0,my_checksum,ID,1)
	bytesInDouble=struct.calcsize("d")
	data=(192-bytesInDouble)*"Q"
	data=struct.pack("d",time.time())+data
	#calculating the checksum on the data on the header
	my_checksum=checksum(header+data)
	#creating the header
	header=struct.pack(
	"blahh",ICMP_ECHO_REQUEST,0,socket.htons(my_checksum),ID,1
	)
	packet=header+data
	s.sendto(packet,(destination_address,1))

#function for creating checksum called in craft_packet function
def checksum(source_string):
	sum1=0
	countTo=(len(source_string)/2)*2
	count=0
	while count<countTo:
		thisVal=ord(source_string[count+1])*256+ord(source_string[count])
		sum1=sum1+thisVal
		sum1=sum1 & 0xffffffff
		count=count+2
	if countTo<len(source_string):
		sum1=sum1+ord(source_string[len(source_string)-1])
		sum1=sum1 & 0xffffffff
	sum1 =(sum1>>16)+(sum1 & 0xffff)
	sum1 =sum1+(sum1>>16)
	answer=~sum1
	answer=answer & 0xffff
	#swapping bytes
	answer=answer>>8 | (answer <<8 & 0xff00)
	return answer
#main function that calls all the other function and display the results
def main_function(destination_address,timeout=2,count=4):
	for i in range(count):
		print "Custom Ping..%s"%destination_address,
		try:
			delay=create()
		except socket.gaierror,e:
			print "Failed(socket error:'%s') "%e[1]
			break
		if delay ==None:
			print "failed. (timeout within %ssec)"%timeout
		else:
			delay=delay*1000
			print "get ping in %0.4fms"%delay
	print
if __name__=='__main__':
	main_function(destination_address)

pragma solidity ^0.4.0;

contract PDMAccess{
    
    struct User{
        string userName;
        string password;
    }
    
    uint userIndex = 0;
    
    mapping(uint => User) users;
    
    bool userExists = false;
    
    function addUser(string _username, string _password){
        for(uint i = 0;i < userIndex;i++){
            if(keccak256(users[i].userName) == keccak256(_username)){
                userExists = true;
                break;
            }
        }
        if(!userExists){
            users[userIndex].userName = _username;
            users[userIndex].password = _password;
            userIndex++;
        }
        else
            throw;
    }
    
    function getUser(uint n) constant returns(string _username, string _password){
        if(n < userIndex){
            _username = users[n].userName;
            _password = users[n].password;
        }
        else
            throw;
    }
    
    function isUser(string _username, string _password) constant returns(bool verify){
        verify = false;
        for(uint iter = 0;iter < userIndex;iter++){
            if((keccak256(users[iter].userName)==keccak256(_username)) && (keccak256(users[iter].password)==keccak256(_password))){
                verify = true;
                break;
            }
        }
    }
    
    function updateUserPassword(string _username, string _oldpassword, string _newpassword){
        for(uint iter = 0;iter < userIndex;iter++){
            if((keccak256(users[iter].userName)==keccak256(_username)) && (keccak256(users[iter].password)==keccak256(_oldpassword))){
                users[iter].password = _newpassword;
            }
        }
    }
        
}

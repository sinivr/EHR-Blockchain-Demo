pragma solidity ^0.4.11;

import "github.com/Arachnid/solidity-stringutils/strings.sol";

contract PdmAnthem {
    using strings for *;

    struct PdmAnthem {
        string first_Business_Name;
        string lastName;
		string npi;
		string specialityName;
		string state;
		string userID;
		string data;
    }

    PdmAnthem[] public pdmAnthems;

    function addPdmAnthem( string _first_Business_Name, string _lastName, string _npi, string _specialityName, string _state,string _userID,  string _data) public returns(uint) {
        for(uint iter = 0;iter < pdmAnthems.length;iter++){
			if(keccak256(pdmAnthems[iter].npi) == keccak256(_npi)){
				throw;
			}
		}
		
		pdmAnthems.length++;
        pdmAnthems[pdmAnthems.length-1].first_Business_Name = _first_Business_Name;
        pdmAnthems[pdmAnthems.length-1].lastName = _lastName;
		pdmAnthems[pdmAnthems.length-1].npi = _npi;
		pdmAnthems[pdmAnthems.length-1].specialityName = _specialityName;
		pdmAnthems[pdmAnthems.length-1].state = _state;
		pdmAnthems[pdmAnthems.length-1].userID = _userID;
		pdmAnthems[pdmAnthems.length-1].data = _data;
		
        return pdmAnthems.length;
    }

    function updatePdmAnthem(uint _idNum, string _first_Business_Name, string _lastName, string _npi, string _specialityName, string _state,string _userID, string _data) public returns(uint) {
        pdmAnthems[_idNum].first_Business_Name = _first_Business_Name;
        pdmAnthems[_idNum].lastName = _lastName;
		pdmAnthems[_idNum].npi = _npi;
		pdmAnthems[_idNum].specialityName = _specialityName;
		pdmAnthems[_idNum].state = _state;
		pdmAnthems[_idNum].userID = _userID;
		pdmAnthems[_idNum].data = _data;
		
        return pdmAnthems.length;
    }

    function getPdmAnthemsCount() public constant returns(uint) {
        return pdmAnthems.length;
    }

    function getPdmAnthem(uint index) public constant returns(string, string, string, string, string, string, string) {
        return ( pdmAnthems[index].first_Business_Name, pdmAnthems[index].lastName, pdmAnthems[index].npi, pdmAnthems[index].specialityName, pdmAnthems[index].state,pdmAnthems[index].userID, pdmAnthems[index].data);
    }
	
	function getIndProvider(string ipuserID,string ipnpi) constant returns(string out)
	{
	    out = out.toSlice().concat("[".toSlice());
		bool appendComma=false;
	    for(uint iter = 0;iter < pdmAnthems.length;iter++){
	        if((keccak256(ipuserID)==keccak256(pdmAnthems[iter].userID)) && (keccak256(ipnpi)==keccak256(pdmAnthems[iter].npi))){
			if(appendComma){
	                    out = out.toSlice().concat(",".toSlice());
	                }
	                out = out.toSlice().concat(pdmAnthems[iter].data.toSlice());
					appendComma=true;	        }
	    }
		 out = out.toSlice().concat("]".toSlice());
	    return out;

	}
	
	function searchIndProviderPdm(string ipFirstName, string ipSpecialityName, string ipNpi) constant returns(string out)
	{
         out = out.toSlice().concat("[".toSlice());
			bool appendComma=false;
	    if(keccak256(ipFirstName)==keccak256("NONE")){
	        for(uint iter = 0;iter < pdmAnthems.length;iter++){
	            if(keccak256(pdmAnthems[iter].specialityName)==keccak256(ipSpecialityName) && keccak256(pdmAnthems[iter].npi)==keccak256(ipNpi)){
					if(appendComma){
	                    out = out.toSlice().concat(",".toSlice());
	                }
	                out = out.toSlice().concat(pdmAnthems[iter].data.toSlice());
					appendComma=true;
	            }
	        }
	    }
	    else{
	        for(uint iter2 = 0;iter2 < pdmAnthems.length;iter2++){
	            if(keccak256(pdmAnthems[iter2].specialityName)==keccak256(ipSpecialityName) && keccak256(pdmAnthems[iter2].npi)==keccak256(ipNpi) && keccak256(pdmAnthems[iter2].first_Business_Name)==keccak256(ipFirstName)){
					if(appendComma){
	                    out = out.toSlice().concat(",".toSlice());
	                }
	                out = out.toSlice().concat(pdmAnthems[iter2].data.toSlice());
					appendComma=true;
	            }
	        }
	    }
	   
 out = out.toSlice().concat("]".toSlice());
	    return out;
	    
	}
	
	
	function searchIndProviderPayer(string inputFirstName, string inputstate, string inputSpecialityName) constant returns(string out){
        out = out.toSlice().concat("[".toSlice());
			bool appendComma=false;
	    if(keccak256(inputFirstName)==keccak256("NONE")){
	        for(uint iter = 0;iter < pdmAnthems.length;iter++){
	            if((keccak256(pdmAnthems[iter].specialityName) == keccak256(inputSpecialityName)) && (keccak256(pdmAnthems[iter].state) == keccak256(inputstate))){
	                if(appendComma){
	                    out = out.toSlice().concat(",".toSlice());
	                }
	                out = out.toSlice().concat(pdmAnthems[iter].data.toSlice());
					appendComma=true;
	            }
	        }
	    }
	    else{
	        for(uint iter2 = 0;iter2 < pdmAnthems.length;iter2++){
	             if((keccak256(pdmAnthems[iter].specialityName) == keccak256(inputSpecialityName)) && (keccak256(pdmAnthems[iter].state) == keccak256(inputstate))){
	                if(appendComma){
	                    out = out.toSlice().concat(",".toSlice());
	                }
	                out = out.toSlice().concat(pdmAnthems[iter].data.toSlice());
					appendComma=true;
	            }
	        }
	    }
	    out = out.toSlice().concat("]".toSlice());
	    return out;
	}
	
}
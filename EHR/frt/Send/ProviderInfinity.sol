pragma solidity ^0.4.11;

import "github.com/Arachnid/solidity-stringutils/strings.sol";

contract ProviderInfinity {
    using strings for *;

    struct Provider {
        string first_business_Name;
        string status;
		string npi;
		string specialityName;
		string state;
		string password;
		string data;
    }

    Provider[] public providers;

    function addProvider(string _first_business_Name, string _npi, string _specialityName, string _state, string _password, string _data) public returns(uint) {
        for(uint iter = 0;iter < providers.length;iter++){
			if(keccak256(providers[iter].npi) == keccak256(_npi)){
				throw;
			}
		}
		providers.length++;
        providers[providers.length-1].first_business_Name = _first_Business_Name;
        providers[providers.length-1].status = "Pending";
		providers[providers.length-1].npi = _npi;
		providers[providers.length-1].specialityName = _specialityName;
		providers[providers.length-1].state = _state;
		providers[providers.length-1].password = _password;
		providers[providers.length-1].data = _data;
		
        return providers.length;
    }

    function getProvider(string ippassword,string ipnpi) constant returns(string out_first_business_Name, string out_npi, string out_specialityName, string out_state, string out_password, string out_data, uint out_index)
	{
		for(uint iter = 0;iter < providers.length;iter++)
		{
			if(keccak256(providers[iter].npi) == keccak256(ipnpi) && keccak256(providers[iter].password) == keccak256(ippassword)){

				out_first_business_Name = providers[iter].first_business_Name;
				out_npi = providers[iter].npi;
				out_specialityName = providers[iter].specialityName;
				out_state = providers[iter].state;
				out_password = providers[iter].password;
				out_data = providers[iter].data;
				out_index = iter;
			}

			else throw;
		}
	}

    function updateProvider(uint _index, string _first_business_Name, string _npi, string _specialityName, string _state, string _password, string _data) public returns(uint) {

		providers[_index].first_Business_Name = _first_business_Name;
		providers[_index].status = "Pending";
		providers[_index].npi = _npi;
		providers[_index].specialityName = _specialityName;
		providers[_index].state = _state;
		providers[_index].password = _password;
		providers[_index].data = _data;
    }
    
	function searchProviderPayer(string inputfirstName, string inputstate, string inputspecialityName) constant returns(string out){
        out = out.toSlice().concat("[".toSlice());
			bool appendComma=false;
	    if(keccak256(inputfirstName)==keccak256("NONE")){
	        for(uint iter = 0;iter < providers.length;iter++){
	            if((keccak256(providers[iter].specialityName) == keccak256(inputspecialityName)) && (keccak256(providers[iter].state) == keccak256(inputstate))){
	                if(appendComma){
	                    out = out.toSlice().concat(",".toSlice());
	                }
	                out = out.toSlice().concat(providers[iter].data.toSlice());
					appendComma=true;
	            }
	        }
	    }
	    else{
	        for(uint iter2 = 0;iter2 < providers.length;iter2++){
	             if((keccak256(providers[iter].specialityName) == keccak256(inputpsecialityName)) && (keccak256(providers[iter].state) == keccak256(inputstate))){
	                if(appendComma){
	                    out = out.toSlice().concat(",".toSlice());
	                }
	                out = out.toSlice().concat(providers[iter].data.toSlice());
					appendComma=true;
	            }
	        }
	    }
	    out = out.toSlice().concat("]".toSlice());
	    return out;
	}
	
}